fn main() {
    
    // ~ This is an example of the  factor that data is natively immutable
    // ~ x is created and equals 5, is printed and then reset to 6, but this is NOT allowed
    // ~ once created an imutate object can NOT be changed

    // let x = 5;
    // println!("The value of x is: {}", x);
    // x = 6;
    // println!("The value of x is: {}", x);

    // ~Below the code WILL compile because x is instantiated as a MUTABLE object
    // ~Thus when it is revalued it is allowed. 

    // let mut x = 5;
    // println!("The value of x is: {}", x);
    // x = 6;
    //println!("The value of x is: {}", x);

    // ~Constants are ALWAYS immutable and MUST be annotated (: u32 etc)

    // const MAX_POINTS: u32 = 100_000;

    // ~Shadowing, by repeating let the value of x is written over, not "changed"
    // ~This is legal and WILL compile

    // let x = 5;

    // let x = x + 1;

    // let x = x * 2;

    // println!("The value of x is: {}", x);

    // ~This is an example of how to change the TYPE of a variable through shadowing
    // ~The variable "spaces" is shadowed to change from "string" to "numerical"

    // let spaces = "   ";
    // let spaces = spaces.len();

    // ~The below would result in a compile error because type is immutable
    
    // let mut spaces = "   ";
    // spaces = spaces.len();

    println!("Variables are normally imutable, but can be changed by shadowing or specifying mut. 
    Additionally constants are always imutable and variable type can not be changed except by shadowing.");
}